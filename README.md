# ellipsis-mindconnect

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Provides a command for scanning an incoming directory for files, processing them and sending data to Mindasphere.

This script manages the interpretation and flow of data files to be sent to Mindsphere.
Assets are configured such that incoming directories are watched, and any new files coming in are processed.
Incoming files are placed in the ./in/assetname directory, processed files are automatically moved to the ./out/assetname directory.
Configuration of a new asset requires addition of an asset to the asset array in a call to `initAgents()`.
The script is designed to be run as a background process.

## Table of Contents

- [ellipsis-mindconnect](#ellipsis-mindconnect)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
    - [Configuration](#configuration)
    - [Asset Creation in Mindsphere](#asset-creation-in-mindsphere)
    - [Asset Onboarding](#asset-onboarding)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
git clone https://bitbucket.org/ellipsistechnology/ellipsis-mindconnect.git
npm i
```

## Usage

```
npm run start
```

### Configuration

Configuration is specified in a call to the `initAgents()` function:

```
initAgents({
    "scanInterval": 60000,
    "assets": [
        {
            "name": name of asset,
            "id": asset ID in Mindsphere,
            "dataMap": callback, // callback must be a function that will take a string and convert it to JSON
            "onboardingKey": copy from Mindsphere agent
        }
    ]
})
```

Assets are specified in the `assets.json` file which containts an array of assets.
Each asset must include a name and id, and if not yet onboarded will require an onboardingKey from the Mindsphere Agent.
See [Asset Creation in Mindsphere](#asset-creation-in-mindsphere) below for details on how to create the asset and agent in Mindsphere.
See [Asset Onboarding](#asset-onboarding) below for details on how to onboard an asset via an Agent in Mindsphere.

Asset structure:

|Key|Description|
 |---|---|
 |`name`|The name of the asset. This must match the name of the data map in [`config.js`](#configuration).|
 |`id`|The id of the Asset as specified in Mindasphere.|
 |`dataMap`|A map of key-value pairs where the key is the name of the asset and the value is a function that takes a string as its only parameter and returns a JSON object.|
 |`onboardingKey`|The onboarding key of the agent found in Mindsphere.|

Example asset configuration and scan start:

```
const { initAgents, startScanning } = require('ellipsis-mindconnect')
const { etongueToJson } = require('./etongue.js')

initAgents({
    "scanInterval": 60000,
    "assets": [
        {
            "name": "etongue",
            "id": "86f361a751e249aaa893009981b403fa",
            "dataMap": etongueToJson,
            "onboardingKey": {  "content": {    "baseUrl": "https://southgate.eu1.mindsphere.io",    "iat": "eyJraWQiOiJrZXktaWQtMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0...",    "clientCredentialProfile": [      "SHARED_SECRET"    ],    "clientId": "2a9ab8115d3c40eb98776d31f204531e",    "tenant": "utasiot"  },  "expiration": "2020-11-05T08:28:46.000Z"}
        }
    ]
})
.then(startScanning)
.catch(console.error)
```

### Asset Creation in Mindsphere

1. Log into Mindsphere and open the AssetManager
2. Create an aspect:
    * Select Aspects from the left menu.
    * Create a new aspect.
    * Add you variables.
3. Create a Type:
    * Select types from left menu.
    * Create a new type.
    * Select an aspect - make sure you use the dynamic category in order to send time series data to the agent.
4. Create an asset:
    * Select assets from the left menu.
    * If you want to group your assets create an asset and select the Area asset type to use as a hierarchy node and navigate into this new area by clicking on the right arrow in a circle next to it after you've created it.
    * Create an asset and select the type you created above.
    * Find the asset id and copy it into the id field int the `id` field in the asset in the `assets.json` file (you can find the asset's ID in the URL, look for the `selected` query parameter).
5. Create an agent:
    * Select assets from the left menu (if you're not already there).
    * Create a new child asset (select the circled + icon in the top-right).
    * Select MindConnectLib.
    * Fill in the details for name etc. and save.
    * Select the new agent and click on the MindConnectLib arrow down the bottom.
    * Select the security you want (shared secret is easier, RSA_3072 is more secure).

### Asset Onboarding

Agents will be automatically onboarded (if not already onboarded) when the script runs. This requires that the initial access token (IAT) be created and copied into the `assets.json` file as follows:

1. Select assets in the left menu.
2. Select the new agent and click on the MinfConnectLib arrow down the bottom.
3. Click the gear button and press Generate onboarding key.
4. Copy the entire JSON object and paste it into the `onboardingKey` field in the asset in the `assets.json` file.

## Maintainers

[@ellipsistechnology](https://bitbucket.org/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Ellipsis Technology
