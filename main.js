const { initAgents, startScanning } = require('ellipsis-mindconnect')
const { bevscanToJson } = require('./bevscan.js')

initAgents({
    "scanInterval": 60000,
    "assets": [
        {
            "name": "bevscan",
            "id": "701f31972bd845ffa05256d73d187df2",
            "fileMap": (data) => {
                const json = bevscanToJson(data)
                return {
                    filename: json.Filename.substr(json.Filename.lastIndexOf('\\')+1),
                    data: json
                }
            },
            "dataMap": (data) => {
                const json = bevscanToJson(data)
                return {
                    scanName: json['Filename'].substr(json['Filename'].lastIndexOf('\\')+1),
                    date: json['Date'],
                    sampleAveraging: json['Sample Averaging'],
                    productTemp: json['Product Temp. (°C)'],
                    boardTemp: json['Board Temp. (°C)'],
                    arrayTemp: json['Array Temp. (°C)'],
                    setTemp: json['Set Temp. (°C)']
                }
            },
            "onboardingKey": { 
                "content": { 
                    "baseUrl": "https://southgate.eu1.mindsphere.io", 
                    "iat": "eyJraWQiOiJrZXktaWQtMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJTQ0kiLCJzdWIiOiI2OWE0Mzc5NWVhZDM0ZjBjOWY1YTMxOGZiY2U3ZWQ2YyIsImF1ZCI6IkFJQU0iLCJpYXQiOjE2MDY4NTk0MDYsIm5iZiI6MTYwNjg1OTQwNiwiZXhwIjoxNjA3NDY0MjA2LCJqdGkiOiIxZjY3ZDQ2Mi01MGYxLTRlMzgtYmMwMS01ZDlmNzUxNjZkMTYiLCJzY29wZSI6IklBVCIsInRlbiI6InV0YXNpb3QiLCJ0ZW5fY3R4IjoibWFpbi10ZW5hbnQiLCJjbGllbnRfY3JlZGVudGlhbHNfcHJvZmlsZSI6WyJTSEFSRURfU0VDUkVUIl0sInNjaGVtYXMiOlsidXJuOnNpZW1lbnM6bWluZHNwaGVyZTp2MSJdfQ.J7KsZL74mL-hp3yOQQY4ja-JTx7hloqan5BYCBPvl8UsSBKbk5AxI0PRx9dpD3jUNX2bUr-0gF_2Brc_7injsrIDLfaq-9fNv3LHc7Xx2xPdZYggU2l-ITF2Hu5oc_3-lqP4Hql4wW0axyoawjBYQkUZA-eXUPCGsBRY--qvKUHUD74Ya9GTBDIlHyaHKm13g-D6oe7scBTwB1r2KdIW2FrurkqUnhVqH4g8rqkHhKJD3ut4mWJTF5AI80QDEsPw-7XrFz_wubLaPqreIH7QRwT-tjxFaqGGt464yneTk-qp2gZA7lV5td0X32YaPL2sb1LjYIkpOEh309AbCyr3kQ", 
                    "clientCredentialProfile": ["SHARED_SECRET"], 
                    "clientId": "69a43795ead34f0c9f5a318fbce7ed6c", 
                    "tenant": "utasiot" 
                }, 
                "expiration": "2020-12-08T21:50:06.000Z" 
            }
        }
    ]
})
.then(startScanning)
.catch(console.error)
