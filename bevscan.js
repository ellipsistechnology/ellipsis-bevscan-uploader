// const fs = require("fs") // for testing only - see end of file

exports.bevscanToJson = (bvs) => {
    const data = {
        values: {}
    }
    const rows = bvs.replace(/\r/g, '').split('\n')
    rows.forEach(row => {
        // Process headers:
        if(row.includes(':')) {
            const keyValue = row.match(/(.+?):(.*)/)
            if(keyValue && keyValue.length === 3) {
                if(keyValue[2].startsWith(',')) {
                    keyValue[2] = keyValue[2].substring(1)
                }
                data[keyValue[1].trim()] = keyValue[2].trim()
            }
        // Process spectrum:
        } else if(row.match(/\d+,\s*[.\d]+/)) {
            const keyValue = row.split(',')
            if(keyValue.length === 2) {
                data.values[keyValue[0]] = keyValue[1].trim()
            }
        }
    })

    return data
}

// Uncomment the following to test:
// fs.promises.readFile(`./in/bevscan/cow160001.bvs`, "utf8")
// .then(bvs => {
//     console.log(exports.bevscanToJson(bvs));
// })
// .catch(console.error)
